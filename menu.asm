%macro print 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
%macro read 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data
    mess db '1. área de triángulo.', 10, '2. salir.', 10
    len equ $ - mess
    mess1 db 'Ingrese la base: '
    len1 equ $ - mess1
    mess2 db 'Ingrese la altura: '
    len2 equ $ - mess2
    mess3 db 'El área es: '
    len3 equ $ - mess3

section .bss
    opcion resb 1
    base resb 1
    altura resb 1
    area resb 1

section .text
    global _start:
_start:
    print mess, len
    read opcion, 2
    mov al, [opcion]
    sub al, '0'
    cmp al, 1
    je areas
    cmp al, 0
    jmp salir
areas:
    print mess1, len1
    read base, 2
    print mess2, len2
    read altura, 2

    mov al, [base]
    sub al, '0'
    mov bl, [altura]
    sub bl, '0'
    mul bl
    mov bl, 2
    div bl
    add al, '0'
    mov [area], al
    print mess3, len3
    print area,1

salir: 
    mov eax, 1
    int 80h