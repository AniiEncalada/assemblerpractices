%macro print 2
    mov eax,4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
%macro read 2
    mov   eax, 3
    mov   ebx, 2
    mov   ecx, %1
    mov   edx, %2
    int   80h
%endmacro

section .data
    mess db 10, 'Carpeta o directorio creado',10
    len equ $ - mess
    dir db 'Ingrese el directorio', 10
    lenDir equ $ - dir
    mss db 'Error al escribir',10
    len1 equ $ - mss
    path1 db '/home/maria/Documentos/Assembler/Archivo',0
    path2 db '/home/maria/Documentos/Assembler/Archivo/archivo.txt',0
section .bss
    ;path resb 1
    idArchivo resb 1
    text resb 1
section .text
    global _start:
    
_start:
   ; print dir, lenDir
    ;read path, 50
    mov eax, 39 ;//Servicio para crear un directorio
    mov ebx, path1 ; Define la ruta del servicio
    mov ecx, 0x1FF ;Definir permiso 777
    int 80h

    print mess, len
;________________Leer texto para archivo____________________________________________
    read text, 12
;________________Crear archivo____________________________________________
    mov eax, 8
    mov ebx, path2
    mov ecx, 2;modo de acceso
    mov edx, 0x1ff ;permisos
    int 80h 
;________________Testear archivo____________________________________________
    test eax, eax
    jz Error
    mov dword [idArchivo], eax
;________________Escribir en archivo____________________________________________
    mov eax, 4
    mov ebx, [idArchivo]
    mov ecx, text
    mov edx, 12
    int 80h
    ;________________Cerrar archivo____________________________________________
    mov eax, 6
    mov ebx, [idArchivo]
    mov ecx, 0
    mov edx, 0
    int 80h
    jmp salir
Error:
    print mss, len1
salir:
    mov eax, 1
    int 80h