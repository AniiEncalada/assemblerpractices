;Examen Practico 2
;Tema: PILAS
;03/02/2020
;Definir 5 numeros
;Ordenar de menor a mayor
;Determinar el numero menor
;Determinar el numero mayor
;Escritura 
%macro print 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
;Lectura
%macro read 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
;Intercambiar posiciones entre 2 registros
%macro stack 2
    push %1
    mov %1, %2
    pop %2
%endmacro
section .data
    mess db 'Ingrese un número entero: '
    len equ $ - mess
    mess1 db 'Mayor: '
    len1 equ $ - mess1
    mess2 db 10, 'Menor: '
    len2 equ $ - mess2
    mess3 db 10, 'Números ordenados: ', 10
    len3 equ $ - mess3
    n db 10, '';Salto de línea
section .bss
    n1 resb 1
    n2 resb 1
    n3 resb 1
    n4 resb 1
    n5 resb 1

section .text
    global _start:
_start:
    ;Pedir 5 numeros enteros
    print mess, len
    read n1, 2
    print mess, len
    read n2, 2
    print mess, len
    read n3, 2
    print mess, len
    read n4, 2
    print mess, len
    read n5, 2

    ;Mover némeros a registros
    mov ax, [n1]
    sub ax, '0'
    mov bx, [n2]
    sub bx, '0'
    mov cx, [n3]
    sub cx, '0'
    mov dx, [n4]
    sub dx, '0'
;------------------------------------------------
;Determinar el número menor
mayorA:
    cmp ax, bx
    jl cmp2 ;mientras sea menor el primer número comparado, salta
    stack ax, bx ;Intercambiar posiciones entre 2 registros
cmp2:
    cmp ax, cx
    jl cmp3 ;mientras sea menor el primer número comparado, salta
    stack ax, cx ;Intercambiar posiciones entre 2 registros
cmp3:
    cmp ax, dx
    jl menor ;mientras sea menor el primer número comparado, salta
    stack ax, dx ;Intercambiar posiciones entre 2 registros
menor:
    push dx ;Guardar valor
    mov dx, [n5] ;Obtener el último ingresado por el usuario para comparar
    sub dx, '0'
    cmp ax, dx
    jl mayorD ;mientras sea menor el primer número comparado, salta
    stack ax, dx ;Intercambiar posiciones entre 2 registros

;-------------------------------------------------
;Determinar el número mayor
mayorD:
    mov [n3], cx
    pop cx
    push ax ;Guardar el menor en pila
    mov ax, bx
    mov bx, [n3]
    cmp dx, ax
    jg cmp2d ;mientras sea mayor el primer número comparado, salta
    stack dx, ax ;Intercambiar posiciones entre 2 registros
cmp2d:
    cmp dx, bx
    jg cmp3d ;mientras sea mayor el primer número comparado, salta
    stack dx, bx ;Intercambiar posiciones entre 2 registros
cmp3d:
    cmp dx, cx
    jg medio ;mientras sea mayor el primer número comparado, salta
    stack dx, cx ;Intercambiar posiciones entre 2 registros

;----------Medios------------------
;Ubicar los valores medios en su orden
medio:
    cmp ax, bx
    jl coms ;mientras sea menor el primer número comparado, salta
    stack ax, bx ;Intercambiar posiciones entre 2 registros
coms:
    cmp ax, cx
    jl parU ;mientras sea menor el primer número comparado, salta
    stack ax, cx ;Intercambiar posiciones entre 2 registros
parU:
    cmp bx, cx
    jl presentar ;mientras sea menor el primer número comparado, salta
    stack bx, cx ;Intercambiar posiciones entre 2 registros

;-----------------------------------------
;Presentar resultados
presentar:
    ;Mover números a variables // Rescatar valores
    add ax, '0'
    mov [n2], ax
    pop ax ;Tiene el menor.
    add ax, '0'
    mov [n2], ax
    add bx, '0'
    mov [n3], bx
    add dx, '0'
    mov [n5], dx
    ;pop dx
    add cx, '0'
    mov [n4], cx

    ;Mensajes --------------------------------------
    ;Mayor
    print mess1, len1
    print n5, 1
    ;Menor
    print mess2, len2
    print n1, 1
    ;Secuencia de números
    print mess3, len3
    print n1, 1
    print n, 1 ;salto de línea
    print n2, 1
    print n, 1 ;salto de línea
    print n3, 1
    print n, 1 ;salto de línea
    print n4, 1
    print n, 1 ;salto de línea
    print n5, 1
    print n, 1 ;salto de línea
    
salir:
    mov eax, 1
    int 80h
