;Imprimir mensaje
%macro print 2 
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
;Recibir dato 
%macro read 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
section .data
    mss db '*', 10
section .text
    global _start:
_start:
    mov cx, 20
    jmp principal
principal:
    cmp cx, 0
    jz salir
    jmp imprimir
imprimir:
    dec cx
    push cx
    print mss, 2
    pop cx
    jmp principal
salir:
    mov eax, 1
    int 80h