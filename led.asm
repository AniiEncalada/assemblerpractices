section .data

  timeval:
    tv_sec  dd 0
    tv_usec dd 0

  bmessage  db "1  "
  bmessagel equ $ - bmessage

  emessage  db "0  "
  emessagel equ $ - emessage
  
section .text
global  _start
_start:
led:
  ; print "Sleep"
  mov eax, 4
  mov ebx, 1
  mov ecx, bmessage
  mov edx, bmessagel
  int 80h

  ; Sleep for 2 seconds and 0 nanoseconds
  mov dword [tv_sec], 2
  mov dword [tv_usec], 0
  mov eax, 162
  mov ebx, timeval
  mov ecx, 0
  int 80h

  ; print "Continue"
  mov eax, 4
  mov ebx, 1
  mov ecx, emessage
  mov edx, emessagel
  int 80h

    ; Sleep for 2 seconds and 0 nanoseconds
  mov dword [tv_sec], 2
  mov dword [tv_usec], 0
  mov eax, 162
  mov ebx, timeval
  mov ecx, 0
  int 80h
jmp led
  ; exit
  mov eax, 1
  mov ebx, 0
  int 80h