%macro print 2
    mov eax,4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
%macro read 2
    mov   eax, 3
    mov   ebx, 2
    mov   ecx, %1
    mov   edx, %2
    int   80h
%endmacro

section .data
    mess db 10, 'Carpeta o directorio creado',10
    len equ $ - mess
    dir db 'Ingrese el directorio', 10
    lenDir equ $ - dir

section .bss
    path resb 1

section .text
    global _start:
    
_start:
    print dir, lenDir
    read path, 50
    mov eax, 39 ;//Servicio para crear un directorio
    mov ebx, path ; Define la ruta del servicio
    mov ecx, 0x1FF ;Definir permiso 777
    int 80h

    print mess, len

    mov eax, 1
    int 80h