%macro print 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
%macro read 2
    mov   eax, 3
    mov   ebx, 2
    mov   ecx, %1
    mov   edx, %2
    int   80h
%endmacro
section .data
    mess1 db 'Numero mayor '
    len1 equ $ - mess1
    mess2 db 'Número menor '
    len2 equ $ - mess2
    mess3 db 'Ingrese el primer número:'
    len3 equ $ - mess3
    mess4 db 'Ingrese el segundo número:'
    len4 equ $ - mess4
    mess5 db 'Iguales '
    len5 equ $ - mess5
section .bss
    n1 resb 1
    n2 resb 1
section .text
    global _start
_start:
    print mess3, len3
    read n1, 2
    print mess4, len4
    read n2, 2

    mov al, [n1]
    sub al, '0'
    mov bh, [n2]
    sub bh, '0'
    cmp al, bh
    jg mayor
    jng menor
    jmp igual
mayor:
    print mess1, len1
    jmp salir
menor:
    print mess2, len2
    jmp salir
igual:
    print mess5, len5
    jmp salir
salir:
    mov eax, 1
    int 80h