%macro print 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
%macro read 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
section .data
    mess db 'Ingrese un numero mayor a 5', 10, 13
    len equ $ - mess
    mess1 db 10,'item '
    len1 equ $ - mess1
section .bss
    n resb 1
section .text
    global _start:
_start:
init:
    print mess, len
    read n, 2

    mov al, [n]
    sub al, '0'
    mov cx, [n]
    cmp al, 5
    jae mayor
    jbe init
mayor:
    cmp cx, 0
    dec cx
    jnz present
    jz salir
present:
    print mess1, len1
    jmp mayor
salir:
    mov eax, 1
    int 80h