%macro show_message 2 
      mov   eax, 4
      mov   ebx, 1
      mov   ecx, %1
      mov   edx, %2
      int   80h
%endmacro

%macro reading 2 
      mov   eax, 3
      mov   ebx, 2
      mov   ecx, %1
      mov   edx, %2
      int   80h
%endmacro

section .data
    msj1 db 'Ingrese el primer número:'
    len1 equ $ - msj1
    msj2 db 'Ingrese el segundo número:'
    len2 equ $ - msj2
    msj3 db 'El cociente de la división es: '
    len3 equ $ - msj3
    msj4 db 10,'El residuo de la división es: '
    len4 equ $ - msj4

section .bss
    n1 resb 1
    n2 resb 1
    cociente resb 1
    residuo resb 1

section .text
    global _start
    _start:

        show_message msj1, len1
        reading n1, 2

        show_message msj2, len2
        reading n2, 2

        ; Division
        mov al, [n1]
        mov bl, [n2]
        sub ax, '0'
        sub bl, '0'
        div bl
        add al, '0'
        add ah, '0'  
        mov [cociente], al
        mov [residuo], ah

        show_message msj3, len3
        show_message cociente, 1

        show_message msj4, len4
        show_message residuo, 1

        mov eax, 1
        int 80h 