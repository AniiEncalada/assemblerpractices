%macro show_message 2 
      mov   eax, 4
      mov   ebx, 1
      mov   ecx, %1
      mov   edx, %2
      int   80h
%endmacro

%macro reading 2 
      mov   eax, 3
      mov   ebx, 2
      mov   ecx, %1
      mov   edx, %2
      int   80h
%endmacro

section .data
    msj1 db 'Ingrese el primer número:'
    len1 equ $ - msj1
    msj2 db 'Ingrese el segundo número:'
    len2 equ $ - msj2
    msj3 db 'El primer número es mayor'
    len3 equ $ - msj3
    cociente db 'El cociente es:'
    lCociente equ $ - cociente
    residuo db 'El residuo es:'
    lresiduo equ $ - residuo
    mayorDivisor db 'El divisor es mayor'
    lmayorDivisor equ $ - mayorDivisor

section .bss
    n1 resb 1
    n2 resb 1
    resta resb 1
    contador resb 1

section .text
    global _start:

_start:
    show_message msj1, len1
    reading n1, 2
    show_message msj2, len2
    reading n2, 2

    mov al, [n1]
    mov bl, [n2]
    mov cx, 0
    sub al, '0'
    sub bl, '0'
    cmp bl, al
    jg mayor
    jmp resta_sucesiva

mayor:
    show_message mayorDivisor, lmayorDivisor
    jmp salir

resta_sucesiva:
    sub al, bl
    inc cx
    cmp al, bl
    je resta_sucesiva
    jg resta_sucesiva
    add al, '0'
    add cx, '0'
    mov [resta], al
    mov [contador], cx
    show_message cociente, lCociente
    show_message contador, 2
    show_message residuo, lresiduo
    show_message resta, 1

salir:
    mov eax, 1
    int 80h 