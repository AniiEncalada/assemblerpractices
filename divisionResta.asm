%macro print 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
%macro read 2
    mov   eax, 3
    mov   ebx, 2
    mov   ecx, %1
    mov   edx, %2
    int   80h
%endmacro
section .data
    mess1 db 'Cociente: '
    len1 equ $ - mess1
    mess2 db 'Residuo: '
    len2 equ $ - mess2
    mess3 db 'Ingrese el primer número:'
    len3 equ $ - mess3
    mess4 db 'Ingrese el segundo número:'
    len4 equ $ - mess4
    mess5 db 'Iguales '
    len5 equ $ - mess5
    cont db 49
section .bss
    n1 resb 1
    n2 resb 1
    c resb 1
    r resb 1
section .text
    global _start
_start:
    print mess3, len3
    read n1, 2
    print mess4, len4
    read n2, 2

    mov cx, 0 
    mov al, [n1]
    sub al, '0'
    mov bl, [n2]
    sub bl, '0'
comparar:
    cmp al, bl
    je restar
    jg restar
    jmp presentar

presentar:
    add al,'0'
	add cx,'0'
    mov [c], cx
    mov [r], al
    print mess1, len1    
    print c, 1
    print mess2, len2
    print r, 2
    jmp salir

restar:
    sub al, bl
    inc cx
    jmp comparar

salir:
    mov eax, 1
    int 80h