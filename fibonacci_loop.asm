section .data
    salto db 10,''
    len_salto equ $-salto

section .bss
    num_a resb 2
    num_b resb 2
    num_c resb 2 ; resultado

section .text
    global _start

_start:
    mov ebx, 0
    mov edx, 1

    add ebx, '0'
    add edx, '0'

    mov [num_a], ebx ; numb_a = 0
    mov [num_b], edx ; num_b = 1
    mov [num_c], ebx ; num_c = 0

    call imprimir_numero ; 0

    mov ecx, 6

    jmp fibonacci

fibonacci:
    push ecx ;5

    mov eax, [num_a] ; eax = 1
    mov ebx, [num_b] ; ebx = 0
    mov ecx, [num_c] ; ecx = 1

    sub eax, '0'
    sub ebx, '0'
    sub ecx, '0'

    mov eax, ebx ; eax = 0
    mov ebx, ecx ; ebx = 1

    mov edx, eax ; edx = 0
    add edx, ebx ; edx = 1
    mov ecx, edx ; ecx = 1

    add eax, '0'
    add ebx, '0'   
    add ecx, '0' 

    mov [num_a], eax ; num_a = 1
    mov [num_b], ebx  ; num_b = 0
    mov [num_c], ecx ; num_c = 1

    call imprimir_numero ;1

    pop ecx ;6

    loop fibonacci ; 5

    jmp salida

imprimir_numero:
    mov eax, 4
    mov ebx, 1
    mov ecx, num_c
    mov edx, 1
    int 80h

    ret

salida:
    mov eax, 1
    int 80h
    