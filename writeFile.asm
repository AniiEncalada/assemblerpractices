%macro print 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
%macro read 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
section .data
    archivo db '/home/maria/Documentos/Assembler/arch.txt',0
    mss db 'Error al escribir',10
    len equ $ - mss
section .bss
    idArchivo resb 1
    text resb 1
section .text
    global _start:
_start:
;________________Leer texto para archivo____________________________________________
    read text, 12
;________________Crear archivo____________________________________________
    mov eax, 8
    mov ebx, archivo
    mov ecx, 1;modo de acceso
    mov edx, 0x1ff ;permisos
    int 80h 
;________________Testear archivo____________________________________________
    test eax, eax
    jz Error
    mov dword [idArchivo], eax
;________________Escribir en archivo____________________________________________
    mov eax, 4
    mov ebx, [idArchivo]
    mov ecx, text
    mov edx, 12
    int 80h
    ;________________Cerrar archivo____________________________________________
    mov eax, 6
    mov ebx, [idArchivo]
    mov ecx, 0
    mov edx, 0
    int 80h
    jmp salir
Error:
    print mss, len
salir:
    mov eax, 1
    int 80h