;Imprimir mensaje
%macro print 2 
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
;Recibir dato 
%macro read 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data
    mess db 'Ingrese un número entre 1 y 9: '
    len equ $ - mess
    messPrime db 'El número ingresado es primo', 10
    lenPrime equ $ - messPrime
    messNP db 'El número ingresado no es primo', 10
    lenNP equ $ - messNP
section .bss
    prime resb 1

section .text
    global _start:
_start:
    ;Recibir número
    print mess, len
    read prime, 2
    ;Inicializar valores
    mov cx, 0
    mov al, [prime]
    sub al, '0'
    ;Valores por defecto//NO se aplica comparación
    cmp al, 1
    je IsPrime
    cmp al, 2
    je IsPrime

    mov bl, 1
    jmp mod

operation:
    inc bl
    div bl
    cmp ah, 0
    je cont ;si es igual, incrementa contador
    jmp mod ;Comparar
    
mod: ;comparar
    cmp al, bl
    jg operation ;Si es mayor, sigue operando
    jmp finish ;Ir a verificar

cont: ;Incrementar el contador
    inc cx
    jmp mod ;Ir a comparar

finish: ;Verificar si el contador excede
    cmp cx, 0 ;Si el valor del contador es más o igual que 1, no es primo
    je IsPrime ;Si es igual
    jg NPrime ;Si es mayor
  
IsPrime: ;Presentar mensaje si es primo
    print messPrime, lenPrime
    jmp salir  

NPrime: ;Presentar mensaje si no es primo
    print messNP, lenNP
    jmp salir

salir: ;Salir del sistema
    mov eax, 1
    int 80h