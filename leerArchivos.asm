%macro print 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
section .data
    archivo db '/home/maria/Documentos/Assembler/areas.asm',0
    mss db 'Error al leer',10
    len equ $ - mss
section .bss
    text resb 1
    idArchivo resb 1
section .text
    global _start:
_start:
;_____________________________________________________________________________________
;Modos de acceso a archivos
;R-only=0
;W-only=1
;RDWR=2
;_____________________________________________________________________________________
    ;________________Acceder archivo____________________________________________ 
    mov eax, 5; se convoca la subrutina al sistema operativo
    mov ebx, archivo; Ruta
    mov ecx, 0;Modo de acceso
    mov edx, 0
    int 80h

    test eax, eax;Verificar
    jz Error
    ;________________Asignar archivo____________________________________________
    mov dword [idArchivo], eax
    ;________________Leer archivo_______________________________________________
    mov eax, 3
    mov ebx, [idArchivo]
    mov ecx, text
    mov edx, 3000
    int 80h
    print text, 3000
    ;________________Cerrar archivo____________________________________________
    mov eax, 6
    mov ebx, [idArchivo]
    mov ecx, 0
    mov edx, 0
    int 80h

    jmp salir
Error:
    print mss, len

salir:
    mov eax, 1
    int 80h

