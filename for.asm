%macro print 2 
      mov   eax, 4
      mov   ebx, 1
      push cx
      mov   ecx, %1
      mov   edx, %2
      int   80h
      pop cx
%endmacro

section .data
    mss db 10, 'item: '
    len equ $ - mss
section .bss
    aux resb 1
section .text
    global _start:
_start:
    mov cx, 10
ciclo:
    cmp cx, 0
    dec cx
    jnz present
    jz salir
present:
    print mss, len
    push cx
    add cx, '0'
    mov [aux], cx
    pop cx
    print aux, 2
    
    jmp ciclo
salir:
    mov eax, 1
    int 80h
    