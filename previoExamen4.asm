%macro print 2
    mov eax,4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
%macro read 2
    mov   eax, 3
    mov   ebx, 2
    mov   ecx, %1
    mov   edx, %2
    int   80h
%endmacro

section .data
    mess db 10, 'Carpeta o directorio creado',10
    len equ $ - mess
    dir db 'Ingrese el directorio', 10
    lenDir equ $ - dir
    mss db 'Error al escribir',10
    len1 equ $ - mss
    path1 db '/home/maria/Documentos/Assembler/Archivo',0
    path2 db '/home/maria/Documentos/Assembler/Archivo/archivo.txt',0
    n1 db '3277'
    n2 db '2566'
    s db ' + '
    s2 db ' = '
    result db '    '
    lenR equ $ - result
section .bss
    ;path resb 1
    idArchivo resb 1
    text resb 1
section .text
    global _start:
    
_start:
   ; print dir, lenDir
    ;read path, 50
    mov eax, 39 ;//Servicio para crear un directorio
    mov ebx, path1 ; Define la ruta del servicio
    mov ecx, 0x1FF ;Definir permiso 777
    int 80h

    print mess, len
;________________Leer texto para archivo____________________________________________
    read text, 12
;________________Crear archivo____________________________________________
    mov eax, 8
    mov ebx, path2
    mov ecx, 2;modo de acceso
    mov edx, 0x1ff ;permisos
    int 80h 
;________________Testear archivo____________________________________________
    test eax, eax
    jz Error
    mov dword [idArchivo], eax
;_____________Realizar suma_______________________________
; longitud del arreglo
    mov esi, 3
    mov ecx, 4
    ; ----------Desactivar el carry----------
    ; Desactivar el bit (estado activo) de la bandera carry
    clc

addProcess:
    ; Accediendo a posicion de la cadena
    mov al, [n1 + esi]
    mov ah, [n2 + esi]
    adc al, ah
    ; Ajustar bcd, trabaja directamente con al
    aaa
    ; Salvando los estados del procesador y mandarlos a pila
    pushf
    ; Trabaja con números binarios, ajusta a decimal
    or al, 30h ; Se convierte en aas
    popf
    ; Moviendo el resultado
    mov [result + esi], al
    ; Decremento la posición
    dec esi
    loop addProcess

;________________Escribir en archivo____________________________________________
    mov eax, 4
    mov ebx, [idArchivo]
    mov ecx, n1
    mov edx, lenR
    int 80h
    mov eax, 4
    mov ebx, [idArchivo]
    mov ecx, s
    mov edx, 3
    int 80h
    mov eax, 4
    mov ebx, [idArchivo]
    mov ecx, n2
    mov edx, lenR
    int 80h
    mov eax, 4
    mov ebx, [idArchivo]
    mov ecx, s2
    mov edx, 3
    int 80h
    mov eax, 4
    mov ebx, [idArchivo]
    mov ecx, result
    mov edx, lenR
    int 80h
    ;________________Cerrar archivo____________________________________________
    mov eax, 6
    mov ebx, [idArchivo]
    mov ecx, 0
    mov edx, 0
    int 80h
    jmp salir
Error:
    print mss, len1
salir:
    mov eax, 1
    int 80h