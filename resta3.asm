%macro print 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
%macro read 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
section .data
    mess db 'Ingrese un número entero: '
    len equ $ - mess
    mess1 db 'Resultado cumple'
    len1 equ $ - mess1
    mess2 db 'Resultado no cumple'
    len2 equ $ - mess2
section .bss
    n1 resb 1
    n2 resb 1
section .text
    global _start:
_start:
    print mess, len
    read n1, 2
    print mess, len
    read n2, 2

    mov al, [n1]
    sub al, '0'
    mov bl, [n2]
    sub bl, '0'

    sub al, bl
    cmp al, 3
    je positive
    jne negative
    
positive:
    print mess1, len1
    jmp salir
negative:
    print mess2, len2
    jmp salir
salir:
    mov eax, 1
    int 80h