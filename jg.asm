%macro show_message 2 
      mov   eax, 4
      mov   ebx, 1
      mov   ecx, %1
      mov   edx, %2
      int   80h
%endmacro

%macro reading 2 
      mov   eax, 3
      mov   ebx, 2
      mov   ecx, %1
      mov   edx, %2
      int   80h
%endmacro

section .data
    msj1 db 'Ingrese el primer número:'
    len1 equ $ - msj1
    msj2 db 'Ingrese el segundo número:'
    len2 equ $ - msj2
    msj3 db 'El primer número es mayor'
    len3 equ $ - msj3
    msj4 db 'El primer número es menor'
    len4 equ $ - msj4
    msj5 db 'Los números son iguales'
    len5 equ $ - msj5

section .bss
    n1 resb 2
    n2 resb 2

section .text
    global _start:

_start:
    show_message msj1, len1
    reading n1, 2
    show_message msj2, len2
    reading n2, 2

    mov al, [n1]
    mov bl, [n2]
    sub al, '0'
    sub bl, '0'
    cmp al, bl
    je igual
    ; jg.- Compara si un numero es mayor o menor
    jg mayor
    jmp menor

igual:
    show_message msj5, len5
    jmp salir

mayor:
    show_message msj3, len3
    jmp salir

menor:
    show_message msj4, len4

salir:
    mov eax, 1
    int 80h 