;Imprimir mensaje
%macro print 2 
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
;Recibir dato 
%macro read 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
segment .data
msg1 db "ingrese cinco numeros y presione enter:", 10
len1 equ $-msg1

msg3 db "el mayor de estos numeros es: "
len3 equ $-msg3

arre db 0,0,0,0,0
la equ $-arre

msg2 db  0xA
lm2 equ $-msg2


segment .bss
n resb 2

segment .text

global _start

_start:

print msg1, len1

mov esi, arre
mov edi, 0

lee:
read n, 2

mov al, [n]
sub al, '0'

mov [esi], al
 
add esi, 1
add edi, 1

cmp edi, la
jb lee


mov esi, arre
mov edi, 0  
mov bl,0

lp:
mov al, [esi]
cmp al, bl
jb reg
mov bl, al

reg:
inc esi
cmp esi, la
jb lp

imprimir:
	add bl, '0'
	mov [n], bl


	mov eax, 4
	mov ebx, 1
	mov ecx, msg3
	mov edx, len3
	int 0x80

	mov eax, 4
	mov ebx, 1
	mov ecx, n
	mov edx, 1
	int 0x80

	mov eax, 4
	mov ebx, 1
	mov ecx, msg2
	mov edx, lm2
	int 0x80

salir:
 mov eax, 1
 xor ebx, ebx
 int 0x80
