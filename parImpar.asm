%macro read 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
%macro print 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data
    mess db 'Ingrese un número impar'
    len equ $ - mess
    mess1 db 'El valor ingresado es par, intente de nuevo.'
    len1 equ $ - mess1
    mess2 db 'Se ha verificado correctamente'
    len2 equ $ - mess2
section .bss
    n resb 1
section .text
    global _start:
_start:
inicio:
    print mess, len
    read n, 2
    mov al, [n]
    sub al, '0'
    mov bl, 2
    div bl
    cmp ah, 0
    jz inicio
    jmp salir
salir:
    print mess2, len2
    mov eax, 1
    int 80h