%macro write 2
	mov eax,4  
	mov ebx,1
	mov ecx,%1
	mov edx,%2
	int 80h
%endmacro

%macro read 2
	mov eax,3  
	mov ebx,2
	mov ecx,%1
	mov edx,%2
	int 80h
%endmacro

section .data
    asterisco db '*'
    vacio db ' '
    new_line db 10,''
    
section .bss
 

section .text
    global _start

_start:

    mov ecx, 9
  

l1:
    push ecx
    call print
    pop ecx
    push ecx

    mov eax, 9
    mov ebx, ecx
    sub eax, ebx 

    cmp ecx, 9
    je l2
    jmp vacios

l2:
    push ecx
    call print_vacio
    pop ecx

    loop l2

    pop ecx
    loop l1
    write new_line, 1
    jmp salir

vacios:
    push eax
    push ecx
    write asterisco, 1
    pop ecx
    pop eax
    dec eax
    cmp eax, 0
    jz l2
    jnz vacios


print:
    mov eax, 4
    mov ebx, 1
    mov ecx, new_line
    mov edx, 1
    int 80h

    ret 

print_vacio:
    mov eax, 4
    mov ebx, 1
    mov ecx, vacio
    mov edx, 1
    int 80h

    ret
            

salir:    
    mov eax, 1
    int 80h