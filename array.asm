;Imprimir mensaje
%macro print 2 
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
;Recibir dato 
%macro read 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
section .data
    mess db 'Ingrese un numero entero',10
    lenMess equ $ - mess
    array db 0,0,0,0,0
    len equ $ - array
section .bss
    n resb 1
section .text
    global _start:
_start:
    mov esi, array
    mov edi, 0
get:
    print mess, lenMess
    read n, 2
    mov al, n
    sub al, '0'
    mov [esi], al
    add esi, 1
    add edi, 1
    ;Comparar
    cmp edi, len
    jb get
    mov esi, array
    mov edi, 0
present:
    mov al, [esi]
    add al, '0'
    mov n, al
    print al, 1
    inc esi
    inc edi
    cmp edi, len
    jb present
    mov eax, 1
    int 80h