;Examen 3 | 20/02/2020
;Encalada Maria
;Sexto ciclo
;Convertir número de decimal a binario
%macro print 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1 
    mov edx, %2
    int 80h
%endmacro

%macro read 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data
    mss db 'Ingrese un número: '
    len equ $ - mss
    mss1 db 'Resultado: '
    len1 equ $ - mss1

section .bss
    n resb 1

section .text
    global _start:

_start:
    print mss, len
    read n, 1
    mov al, [n]
    sub al, '0'
    mov bl, 2
    mov cx, 3

compare:
    cmp al, bl
    jl imprimir
    jg operar
    je operar
    
    
operar:
    div bl
    push ax
    jmp compare

imprimir:
    pop ax
    add al, '0'
    mov [n], al
    push ax
    push cx
    print mss1, len1
    print n, 1
    pop cx

imprimirC:
    pop ax
    add ah,'0'
    mov [n], ah
    push cx
    print n, 1
    pop cx
    loop imprimirC
    
    mov eax, 1
    int 80h