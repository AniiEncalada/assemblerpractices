;Ensamblador | 17/02/2020
;Encalada Maria
;Arreglos
;Determinar el menor de 6 números
%macro print 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1 
    mov edx, %2
    int 80h
%endmacro

%macro read 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro

section .data
    msj db "Ingrese 6 numeros enteros: ", 10
    len equ $-msj
    mss db 10,''
    msj1 db "El menor es: "
    len1 equ $-msj1
    arreglo db 0,0,0,0,0,0
    len_arreglo equ $-arreglo 

section .bss
    numero resb 2
    m resb 2

section .text
    global _start

_start:
    print msj, len
    
    ;Asignacion del arreglo en las posiciones efectivas
    mov esi, arreglo
    mov edi, 0

read1:
    read numero, 2
    ;asignacion de un numero en el arreglo
    mov al, [numero]
    sub al, '0'
    mov [esi], al

    inc esi
    inc edi
    cmp edi, len_arreglo
    je reiniciar
    jb read1

reiniciar:
    mov esi, arreglo
    mov edi, 0
    mov al, [esi]
    mov [numero], al
    mov cx, 0

    jmp comparar

comparar:
    
    mov al, [numero]
    add esi, 1
    add edi, 1
    mov bl, [esi]
    cmp al, bl
    jg mayor 
    je mayor
    jmp menor

mayor: 
    mov [numero], bl
    cmp edi, len_arreglo
    je presentar
    jmp comparar

menor: 
    
    cmp edi, len_arreglo
    je presentar
    jmp comparar

presentar:
    mov al, [numero]
    add al, '0'
    mov [m], al
    print msj1, len1
    print m, 1
    print mss,1
salir:
    mov eax, 1
    int 80h
