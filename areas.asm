%macro show_message 2 
      mov   eax, 4
      mov   ebx, 1
      mov   ecx, %1
      mov   edx, %2
      int   80h
%endmacro

%macro reading 2 
      mov   eax, 3
      mov   ebx, 2
      mov   ecx, %1
      mov   edx, %2
      int   80h
%endmacro

section .data
    msj db 'Operaciones de Areas', 10
    len equ $ - msj
    mCuadrado db 'Ingrese la longitud de un lado:'
    lCuadrado equ $ - mCuadrado
    mBRectangulo db 'Ingrese la base:'
    lBRectangulo equ $ - mBRectangulo
    mARectangulo db 'Ingrese la altura:'
    lARectangulo equ $ - mARectangulo
    mRCirculo db 'Ingrese el radio:'
    lRCirculo equ $ - mRCirculo
    mOpcion db '1. Cuadrado', 10, '2. Circulo', 10, '3. Rectángulo', 10,'4. Salir', 10, 'Elija una opcion:'
    lOpcion equ $ - mOpcion
    mAreaC db 10, 'El área del cuadrado es: '
    lAreaC equ $ - mAreaC
    mAreaCir db 10,'El área del círculo es: '
    lAreaCir equ $ - mAreaCir
    mAreaR db 10,'El área del rectangulo es: '
    lAreaR equ $ - mAreaR
    pi db '3.1415'
    lPi equ $ - pi
    final db 10,'Saliendo del programa     '
    lfinal equ $ - final
    
section .bss
    n1 resb 1
    n2 resb 1
    opcion resb 1
    cociente resb 1
    residuo resb 1
    suma resb 1
    resta resb 1
    producto resb 1

section .text
    global _start
    _start:
        show_message msj, len
        
        show_message mOpcion, lOpcion
        reading opcion, 2
        mov bl, [opcion]
        sub bl, '0'
        
        cmp bl, 1
        je cuadrado
        cmp bl, 2
        je circulo
        cmp bl, 3
        je rectangulo
        cmp bl, 4
        je salir

cuadrado:  
        ;CUADRADO
        show_message mCuadrado, lCuadrado
        reading n1, 2       
        mov al, [n1]
        mov bl, [n1]
        sub al, '0'
        sub bl, '0'
        mul bl
        add al, '0'
        mov [producto], al
        show_message mAreaC, lAreaC
        show_message producto, 1
        jmp salir
        
circulo:
        ;CIRCULO
        show_message mRCirculo, lRCirculo
        reading n1, 2       
        mov al, [n1]
        mov bl, [n1]
        sub al, '0'
        sub bl, '0'
        mul bl
        mov bl, [pi]
        sub bl, '0'
        mul bl
        add al, '0'
        mov [producto], al
        show_message mAreaCir, lAreaCir
        show_message producto, 1
        jmp salir

rectangulo:
        ;RECTANGULO
        show_message mBRectangulo, lBRectangulo
        reading n1, 2
        show_message mARectangulo, lARectangulo
        reading n2, 2
        mov al, [n1]
        mov bl, [n2]
        sub ax, '0'
        sub bl, '0'
        mul bl
        add al, '0'
        mov [producto], al
        show_message mAreaR, lAreaR
        show_message producto, 1
        jmp salir

salir:
        show_message final, lfinal
        mov eax, 1
        int 80h 