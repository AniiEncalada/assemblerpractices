%macro print 2 
      mov   eax, 4
      mov   ebx, 1
      mov   ecx, %1
      mov   edx, %2
      int   80h
%endmacro

%macro read 2 
      mov   eax, 3
      mov   ebx, 2
      mov   ecx, %1
      mov   edx, %2
      int   80h
%endmacro

section .data
    msj1 db 'Ingrese un número: '
    len1 equ $ - msj1
    msj2 db 'El número es: '
    len2 equ $ - msj2
    array db 0,0,0,0,0
    len_array equ $ - array

section .bss
    number resb 1

section .text
    global _start:

_start:
    mov esi, array
    mov edi, 0

input:
    print msj1, len1
    read number, 2
    mov al, [number]
    sub al, '0'
    mov [esi], al

    add esi, 1
    add edi, 1

    cmp edi, len_array
    jb input

    mov esi, array
    mov edi, 0 

print_result:
    mov al, [esi]
    add al, '0'
    mov [number], al
    print number, 1

    add esi, 1
    add edi, 1

    cmp edi, len_array
    jb print_result ;Se trabaja con la bandera de carry

salir:
    mov eax, 1
    int 80h