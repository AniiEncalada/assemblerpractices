%macro read 2
    mov eax, 3
    mov ebx, 2
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
%macro print 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1
    mov edx, %2
    int 80h
%endmacro
%macro move 2
    ;transformar a número
    mov ax, [%1]
    sub ax, '0'
    mov bx, [%2]
    sub bx, '0'
%endmacro
section .data
    mess db 'Ingrese número entero: '
    len equ $ - mess
    mess2 db 10
section .bss
    n1 resb 1
    n2 resb 1
    suma resb 1
    resta resb 1
    producto resb 1
    cociente resb 1
    residuo resb 1
section .text
  global _start:
_start:
    ;Recibir datos
    print mess, len
    read n1, 2
    print mess, len
    read n2, 2

    ;SUMA
    move n1, n2
    add ax, bx
    add ax, '0'
    mov [suma], ax
    print suma, 1

    ;RESTA
    move n1, n2
    sub ax, bx
    add ax, '0'
    mov [resta], ax
    print resta, 1

    ;PRODUCTO
    move n1, n2
    mul bx
    add ax, '0'
    mov [producto], ax
    print producto, 1
    print mess2, 1

    ;DIVISIÓN
    ;transformar a número
    mov al, [n1]
    sub al, '0'
    mov bl, [n2]
    sub bl, '0'
    div bl
    add al, '0'
    add ah, '0'
    mov [cociente], al
    mov [residuo], ah
    print cociente, 1
    print residuo, 1
salir:
    mov eax, 1
    int 80h