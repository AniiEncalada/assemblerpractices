%macro print 2 
      mov   eax, 4
      mov   ebx, 1
      mov   ecx, %1
      mov   edx, %2
      int   80h
%endmacro

%macro read 2 
      mov   eax, 3
      mov   ebx, 2
      mov   ecx, %1
      mov   edx, %2
      int   80h
%endmacro

section .data
    mssNegative db 'El resultado es negativo',10
    lenNegative equ $ - mssNegative
    mssPositive db 'El resultado es positivo',10
    lenPositive equ $ - mssPositive
    mss1 db 'Ingrese un número entero '
    len equ $ - mss1
section .bss
    n1 resb 1
    n2 resb 1
section .text
    global _start:
_start:
    print mss1, len
    read n1, 2
    print mss1, len
    read n2, 2

    mov ah, [n1]
    mov bh, [n2]
   ; sub ah, '0'
    ;sub bh, '0'
    sub ah, bh ;Produce cambios en las banderas
    js negative 
    jmp positive

negative:
    print mssNegative, lenNegative
    jmp salir

positive:
    print mssPositive, lenPositive
    jmp salir

salir:
    mov eax, 1
    int 80H

