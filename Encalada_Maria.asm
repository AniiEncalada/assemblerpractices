;Examen 5 | 16/03/2020
;Encalada Maria
;Sexto ciclo
;Arreglos y archivos
;Se supone que el resultado es de 2 cifras. Para resultados mayores da '00'
%macro print 2
    mov eax, 4
    mov ebx, 1
    mov ecx, %1 
    mov edx, %2
    int 80h
%endmacro

section .data
    path db "/home/maria/Documentos/Assembler/archivo.txt",0
    mss1 db "Error al leer el archivo",10;en caso de error
    len1 equ $- mss1
    mssA db 'Arreglo',10
    lenA equ $ - mssA
    mss db " se repite "
    len equ $-mss
    messV db ' veces.'
    lenV equ $ - messV
    mss2 db "La suma de todos los número es: "
    len2 equ $-mss2

    result db "  ";se espera resultado de 2 cifras
    newLine db 10,''

section .bss
    idFile resb 1
    text resb 20
    stateActual resb 1
    aux resb 1
    sTotal resb 1

section .text
    global _start

_start:
    mov eax, 5 ;Se convoca una subrutina de lectura al SO
    mov ebx, path ;Dirección del archivo
    ;Modos de acceso:
    ; Read Only = 0
    ; Write Only = 1
    ; RDWR = 2
    mov ecx, 0
    mov edx, 0 ;Estableciendo permisos
    int 0x80

    ;Verifica si existe o no existe la dirección
    ; Si ZF = 0 existe un error
    test eax, eax 
    jz error

    ;Obtener ID
    mov dword [idFile], eax
    ;Obtener datos
    mov eax, 3
    mov ebx, [idFile];EStandar de lectura
    mov ecx, text
    mov edx, 30
    int 80h

    print mssA, lenA
    print text, 50
    print newLine, 2;salto de línea

    ;Cerrar archivo
    mov eax, 6
    mov ebx, [idFile]
    mov ecx, 0
    mov edx, 0

    ;Acceder a posiciones
    mov esi, text
    mov edi, 0
    
    mov dh, 0
    mov dl, 0

    mov [sTotal], dh;iniciar

    jmp viewEquals;Comparar

error:
    print mss1, len1; mostrar mensaje de error

    jmp exit;salir del programa

viewEquals:;comparar
    mov al, [esi]
    sub al, '0'

    add esi, 1
    add edi, 1

    cmp al, dl
    je increment

    cmp edi, 20
    jb viewEquals

    jmp verificate

increment:;Incremetar para saber cuantas veces se repite
    inc dh

    cmp edi, 20
    jb viewEquals

verificate:
    cmp dh, 0
    jg present;presentar el resultado de la comparación

    inc dl
    mov esi, text
    mov edi, 0    
    mov dh, 0

    cmp dl, 9
    jng viewEquals

    clc

    jmp sumTotal;realiza la suma de todos los valores

present:
    push dx

    add dl, '0';convertir en cadena
    mov [stateActual], dl;pasar a variable

    add dh, '0';convertir en cadena
    mov [aux], dh;pasar a variable

    print stateActual, 1;mostrar el número que se repite
    print mss, len;mensaje
    print aux, 1;cantidad de veces que se repite
    print messV, lenV;fin de cadena muestra
    print newLine, 2;salto de línea

    pop dx
    ;resetear
    inc dl
    mov esi, text
    mov edi, 0    
    mov dh, 0

    cmp dl, 9
    jng viewEquals

    clc
;realizar la suma de todos los números
    jmp sumTotal

sumTotal: ;realizar la suma de todos los números
    mov al, [result + 1]
    mov ah, [esi]

    sub ah, '0';converitr
    adc al, ah

    ; Ajustar bcd, trabaja directamente con al
    aaa
    ; Salvando los estados del procesador y mandarlos a pila
    pushf
    ; Trabaja con números binarios, ajusta a decimal
    or al, 30h ; Se convierte en aas
    popf

    mov [result + 1], al

    mov al, [result + 0]
    mov ah, 0

    adc al, ah

    ; Ajustar bcd, trabaja directamente con al
    aaa
    ; Salvando los estados del procesador y mandarlos a pila
    pushf
    ; Trabaja con números binarios, ajusta a decimal
    or al, 30h ; Se convierte en aas
    popf

    mov [result + 0], al

    add esi, 1
    add edi, 1

    cmp edi, 20
    jb sumTotal
    ;Presentar suma
    print newLine, 2;salto de línea
    print mss2, len2;mensaje
    print result, 2;REsultado de 2 cifras
    print newLine, 2;nueva linea

exit:
    mov eax, 1
    int 80h